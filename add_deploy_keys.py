# from https://stackoverflow.com/questions/2466401/how-to-generate-ssh-key-pairs-with-python/39126754
from cryptography.hazmat.primitives import serialization as crypto_serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.backends import default_backend as crypto_default_backend
import datetime
import traceback
from settings import GITLAB_URL, GITLAB_TOKEN, GITLAB_GROUP_ID

import gitlab

def get_keypair():
    key = rsa.generate_private_key(
        backend=crypto_default_backend(),
        public_exponent=65537,
        key_size=4096,
    )
    private_key = key.private_bytes(
        crypto_serialization.Encoding.PEM,
        crypto_serialization.PrivateFormat.PKCS8,
        crypto_serialization.NoEncryption()
    ).decode('utf-8')
    public_key = key.public_key().public_bytes(
        crypto_serialization.Encoding.OpenSSH,
        crypto_serialization.PublicFormat.OpenSSH
    ).decode('utf-8')
    return private_key, public_key

GL = gitlab.Gitlab(GITLAB_URL, private_token=GITLAB_TOKEN)

gl_projects = GL.groups.get(GITLAB_GROUP_ID).projects.list()

for ro_project in gl_projects:
    print('Adding key to ', ro_project.name)
    project = GL.projects.get(ro_project.id)

    keys = project.keys.list()
    keys_to_delete = [
        key for key in keys if key.title.startswith('TANUKI_KEY ')
    ]

    private_key, public_key = get_keypair()
    project.keys.create({
        'title': 'TANUKI_KEY ' + datetime.datetime.now().isoformat(),
        'key': public_key,
        'can_push': True,
    })

    try:
        print('Trying to remove the old environment variable...')
        project.variables.delete('TANUKI_KEY')
    except:
        pass

    print('Adding environment variable...')
    project.variables.create({
        'key': 'TANUKI_KEY',
        'value': private_key,
        'variable_type': 'file',
        # 'masked': True, errors?
    })

    for key in keys_to_delete:
        print('Deleting old key', key.title)
        key.delete()

    print('Added key to ', ro_project.name)
