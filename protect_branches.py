import os
import gitlab

from settings import GITLAB_URL, GITLAB_TOKEN, GITLAB_GROUP_ID

PROTECTED_BRANCHES = os.getenv('TANUKI_PROTECTED_BRANCHES').split(',')
GL = gitlab.Gitlab(GITLAB_URL, private_token=GITLAB_TOKEN)
gl_projects = GL.groups.get(GITLAB_GROUP_ID).projects.list()

for ro_project in gl_projects:
    project = GL.projects.get(ro_project.id)

    branches = [branch.name for branch in project.protectedbranches.list()]
    for branch in [branch for branch in branches if branch in PROTECTED_BRANCHES]:
        print(f'unprotecting {project.name}/{branch}')
        project.protectedbranches.delete(branch)

    for branch in PROTECTED_BRANCHES:
        print(f'protecting {project.name}/{branch}')
        project.protectedbranches.create({
            'name': branch,
            'merge_access_level': gitlab.DEVELOPER_ACCESS,
            'push_access_level': gitlab.MAINTAINER_ACCESS,
            'code_owner_approval_required': True,
        })
    