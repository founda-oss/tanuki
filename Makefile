build:
	docker build -t tmp -t founda/tanuki .
run:
	docker run --env-file=.env -v$(pwd):/app founda/tanuki
deploy-keys:
	docker run --env-file=.env -v$(pwd):/app founda/tanuki python3 add_deploy_keys.py
branches:
	docker run --env-file=.env -v$(pwd):/app founda/tanuki python3 protect_branches.py
mra:
	docker run --env-file=.env -v$(pwd):/app founda/tanuki python3 merge_request_approvals.py
default-branch:
	docker run --env-file=.env -v$(pwd):/app founda/tanuki python3 default_branch.py
ns-to-group:
	docker run --env-file=.env -v$(pwd):/app founda/tanuki python3 ns_to_group.py

test-on:
	mv -vn .env production.env
	mv -vn test.env .env
test-off:
	mv -vn .env test.env
	mv -vn production.env .env
