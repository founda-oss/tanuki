import os

GITHUB_TOKEN = os.getenv('TANUKI_GITHUB_TOKEN')
GITHUB_ORGANIZATION = os.getenv('TANUKI_GITHUB_ORGANIZATION')
GITHUB_MIRROR_USER = os.getenv('TANUKI_GITHUB_MIRROR_USER') # username:token

GITLAB_TOKEN = os.getenv('TANUKI_GITLAB_TOKEN')
GITLAB_URL = os.getenv('TANUKI_GITLAB_URL', 'https://gitlab.com')
GITLAB_ORGANIZATION = os.getenv('TANUKI_GITLAB_ORGANIZATION', GITHUB_ORGANIZATION)
GITLAB_GROUP_ID = os.getenv('TANUKI_GITLAB_GROUP_ID')
