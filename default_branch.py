import os
import gitlab

from settings import GITLAB_URL, GITLAB_TOKEN, GITLAB_GROUP_ID

DEFAULT_BRANCH = os.getenv('TANUKI_DEFAULT_BRANCH', 'development')
GL = gitlab.Gitlab(GITLAB_URL, private_token=GITLAB_TOKEN)
gl_projects = GL.groups.get(GITLAB_GROUP_ID).projects.list()

for ro_project in gl_projects:
    project = GL.projects.get(ro_project.id)
    print(f'setting default branch for {project.name} to {DEFAULT_BRANCH}')
    project.default_branch = DEFAULT_BRANCH
    try:
        project.save()
    except:
        pass
