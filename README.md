# tanuki

Tiny scripts we are using to migrate from GitHub to GitLab.

Please make sure to understand the code before running it!

## Usage

See env.example for an example .env file.

### Import repositories

```
make build run
```

### Deploy keys

Generate an SSH key for each repository to use as a `TANUKI_KEY` CI environment variable. Re-run to refresh.

**Usecase**: Push from CI to the repository for `lerna`

```
make build deploy-keys
```

### Protected branches

Set the branches set in `TANUKI_PROTECTED_BRANCHES` to protected.

```
make build protected-branches
```


![](https://i.imgur.com/1PJbCf6.png)
[image licensed by creative commons](https://thenounproject.com/term/tanuki/2763191/)
