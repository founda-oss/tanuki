import time
import traceback
import github, gitlab
from settings import (
    GITHUB_TOKEN, GITLAB_TOKEN, GITHUB_MIRROR_USER,
    GITLAB_URL, GITHUB_ORGANIZATION, GITLAB_ORGANIZATION, GITLAB_GROUP_ID
)

GH = github.Github(GITHUB_TOKEN)
GL = gitlab.Gitlab(GITLAB_URL, private_token=GITLAB_TOKEN)


gh_repos = [repo for repo in GH.get_organization(GITHUB_ORGANIZATION).get_repos()]

for repo in gh_repos:
    print('Importing', repo)
    try:
        GL.projects.import_github(GITHUB_TOKEN, repo.id, GITLAB_ORGANIZATION, timeout=120)
        time.sleep(5)
        print('Sleep...')
    except:
        traceback.print_exc()
        time.sleep(1)
        pass

print('Done importing!')

print('Setting up mirroring...')

gl_repos = GL.groups.get(GITLAB_GROUP_ID).projects.list()

for ro_repo in gl_repos:
    repo = GL.projects.get(ro_repo.id)
    print('Mirroring', repo.name)
    repo.import_url = "".join([
      "https://", GITHUB_MIRROR_USER, "@github.com/", GITHUB_ORGANIZATION, "/",
      repo.name,
    ])
    repo.mirror = True
    repo.mirror_trigger_builds = True
    repo.save()

print('Done!')
