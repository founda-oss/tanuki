import os
import gitlab

from settings import GITLAB_URL, GITLAB_TOKEN, GITLAB_GROUP_ID

MERGE_REQUEST_APPROVERS = os.getenv('TANUKI_MERGE_REQUEST_APPROVERS').split(',')
PROTECTED_BRANCHES = os.getenv('TANUKI_PROTECTED_BRANCHES').split(',')

GL = gitlab.Gitlab(GITLAB_URL, private_token=GITLAB_TOKEN)
gl_projects = GL.groups.get(GITLAB_GROUP_ID).projects.list()

for ro_project in gl_projects:
    project = GL.projects.get(ro_project.id)
    
    branches = {
        branch.name: branch.id
        for branch in project.protectedbranches.list()
    }
    print(branches)
    print(f'setting default settings {project.name}')
    p_mras = project.approvals.get()
    p_mras.reset_approvals_on_push = 1
    p_mras.merge_requests_disable_committers_approval = 1
    p_mras.merge_requests_author_approval = 0
    p_mras.approvals_before_merge = 1
    p_mras.save()

    project.approvals.set_approvers(
        reset_approvals_on_push=True,
        merge_requests_disable_committers_approval=True,
        merge_requests_author_approval=False,
    )

    for rule in project.approvalrules.list():
        print('deleting rule', rule.name)
        rule.delete()



    if 'master-branch' not in [p.name for p in project.approvalrules.list()]:
        print(f'adding master approval to specific group {project.name}')
        project.approvalrules.create(dict(
            name='master-branch',
            approvals_required=1,
            protected_branch_ids=[branches['master']],
            group_ids=MERGE_REQUEST_APPROVERS,
        ))

    if 'development-branch' not in [p.name for p in project.approvalrules.list()]:
        print(f'adding development approval to anyone')
        project.approvalrules.create(dict(
            name='development-branch',
            approvals_required=1,
            protected_branch_ids=[branches['development']],
        ))

