import os
import gitlab

from settings import GITLAB_URL, GITLAB_TOKEN, GITLAB_GROUP_ID, GITLAB_ORGANIZATION

NS = os.getenv('TANUKI_NS', 'packages_:packages,services_:services').split(',')
GL = gitlab.Gitlab(GITLAB_URL, private_token=GITLAB_TOKEN)
gl_projects = GL.groups.get(GITLAB_GROUP_ID).projects.list()

for ro_project in gl_projects:
    project = GL.projects.get(ro_project.id)
    for namespace, group in [pair.split(':') for pair in NS]:
        print(namespace, group, project.path)
        if project.path.startswith(namespace):
            project.transfer_project('/'.join([GITLAB_ORGANIZATION, group]))
            project.path.replace(namespace, '')
            project.name.replace(namespace, '')
            project.save()
