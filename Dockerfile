FROM python:3.9-alpine
WORKDIR /app
COPY requirements.txt /app
RUN apk add --virtual=.build-deps gcc musl-dev build-base libffi-dev openssl-dev && \
    apk add --virtual=.run-deps openssh && \
    pip3 install --no-cache-dir -r requirements.txt && \
    apk del .build-deps
    
COPY . /app
CMD python3 app.py
ENV PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1
